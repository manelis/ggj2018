﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUIManager : MonoBehaviour {

    public Transform _hpLocationPlayer;

    public GameObject _hpBar;

    public int _playerNum = 0;

    private List<GameObject> _playerHPSegments;
    private Player _player;

    // Use this for initialization
    void Start()
    {
        if (_playerNum == 1)
            _player = GameplayManager.Instance._player1;
        if (_playerNum == 2)
            _player = GameplayManager.Instance._player2;
        if (_playerNum == 3)
            _player = GameplayManager.Instance._player3;
        if (_playerNum == 4)
            _player = GameplayManager.Instance._player4;


        _playerHPSegments = new List<GameObject>();
        drawHP();
    }

    private void drawHP()
    {
        for (int i = 0; i < _player._current_hp; i++)
        {
            GameObject obj = Instantiate(_hpBar) as GameObject;
            float scale = obj.transform.localScale.x;
            obj.transform.SetParent(_hpLocationPlayer);
            obj.transform.localScale = _hpLocationPlayer.localScale;
            obj.transform.localPosition = new Vector3(-30 / scale, 0, 0) * i;
            _playerHPSegments.Add(obj);
        }
    }

    private void updateHP()
    {
        int index = 1;
        foreach (var item in _playerHPSegments)
        {
            if (index > _player._current_hp)
                item.SetActive(false);
            index++;
        }
    }

    void OnGUI()
    {
        updateHP();
    }
}
