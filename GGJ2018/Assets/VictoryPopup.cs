﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VictoryPopup : MonoBehaviour {

    public Text _text;

    public void enable()
    {
        _text.text = GameplayManager.Instance.playerWon() + " Player Won!";
        gameObject.SetActive(true);
    }

    public void disable()
    {
        gameObject.SetActive(false);
    }

    void OnGUI () {
        transform.localScale += new Vector3(0.1f * Time.deltaTime, 0.1f * Time.deltaTime, 0);
	}
}
