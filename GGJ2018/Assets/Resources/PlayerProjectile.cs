﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProjectile : MonoBehaviour {

    public Player _player;
    public Satellite _targetSatellite;
    public ParticleSystem _particleSystem;
    public TrailRenderer _trailRenderer;

    private Vector3 _startingPosition;
    private float _frameNum = 0;

    private bool _destroyed = false;

	// Use this for initialization
	void Start () {
    }

    public float EaseInQuad(float start, float end, float value)
    {
        end -= start;
        return end * value * value + start;
    }

    // Update is called once per frame
    void Update ()
    {
        if (_destroyed)
            return;

        var currDistance = (_targetSatellite.transform.position - transform.position).magnitude;
        var distance = (_targetSatellite.transform.position - _startingPosition).magnitude + float.Epsilon;

        var easing = EaseInQuad(0.25f, 0.9f, currDistance / distance);
        float easingVelocity = easing * 60;
        transform.position = Vector3.MoveTowards(transform.position, _targetSatellite.transform.position, Time.deltaTime * easingVelocity);

        if(easing < 0.255f)
        {
            GameplayManager.Instance._cameraShake.movementImpact((_targetSatellite.transform.position - _startingPosition).normalized);
            
            GameplayManager.Instance.getSatelliteManager().movePlayerToSatelite(_player, _targetSatellite);
            _player._currentProjectile = null;
            _destroyed = true;
            GameObject.Destroy(this.gameObject, 0.5f);
            GetComponent<AudioSource>().Stop();
        }
    }

    public void setTarget(Satellite goal)
    {
        _startingPosition = transform.position;
        _targetSatellite = goal;
        _particleSystem.transform.LookAt(transform.position + transform.position - _targetSatellite.transform.position);
        _trailRenderer.Clear();
        _trailRenderer.startColor = _player._playerColor;
        _trailRenderer.gameObject.SetActive(true);
    }

}
