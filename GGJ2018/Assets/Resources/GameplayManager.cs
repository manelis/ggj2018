﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager  {

    private static GameplayManager _instance = null;

    private SatelliteManager _satelliteManager;
    public CameraShake _cameraShake;
    private UIManager _uiManager;
    public SoundManager _soundManager;
    public Player _player1;
    public Player _player2;
    public Player _player3;
    public Player _player4;

    public List<int> _position;

    public int _max_hp = 1;

    protected GameplayManager(){
        //init();
    }

    public static GameplayManager Instance{
        get{
            if (_instance == null){
                _instance = new GameplayManager();
            }
            return _instance;
        }
    }

    public void init()
    {
        _satelliteManager = GameObject.Find("Map").GetComponent<SatelliteManager>();

        _player1 = new Player("Horizontal", "Vertical", "Fire1", "Fire2", Color.red, _max_hp);
        _player2 = new Player("horizontal_player_2", "vertical_player_2", "button_player_2", "boom_button_player_2", Color.blue, _max_hp);
        _player3 = null;
        _player4 = null;
        _cameraShake = Camera.main.GetComponent<CameraShake>();
        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
        _soundManager = GameObject.Find("Map").GetComponent<SoundManager>();
        _position = new List<int>();
    }

    public void destroyPlayer(Player destroyer, Player destroyed)
    {
        destroyer.addScore();
        destroyed.die();
    }

    public string playerWon()
    {
        if (!_player1.isDead())
        {
            return "Red";
        }
        if (!_player2.isDead())
        {
            return "Blue";
        }
        if(_player3 != null && _player4 != null)
        {
            if (!_player3.isDead())
            {
                return "Green";
            }
            if (!_player4.isDead())
            {
                return "Yellow";
            }
        }

        return "0";
    }

    public bool gameEnded()
    {
        if (_player3 == null && _player4 == null)
            return _player1.isDead() || _player2.isDead();

        return ((_player1.isDead() && _player2.isDead() && _player3.isDead()) ||
            (_player1.isDead() && _player3.isDead() && _player4.isDead()) ||
            (_player1.isDead() && _player2.isDead() && _player4.isDead()) ||
            (_player2.isDead() && _player3.isDead() && _player4.isDead()));
    }

    public void restartGame()
    {
        init();
    }

    public void restartGame4Players()
    {
        _satelliteManager = GameObject.Find("Map").GetComponent<SatelliteManager>();

        _player1 = new Player("Horizontal", "Vertical", "Fire1", "Fire2", Color.red, _max_hp);
        _player2 = new Player("horizontal_player_2", "vertical_player_2", "button_player_2", "boom_button_player_2", Color.blue, _max_hp);
        _player3 = new Player("horizontal_player_3", "vertical_player_3", "button_player_3", "boom_button_player_3", Color.green, _max_hp);
        _player4 = new Player("horizontal_player_4", "vertical_player_4", "button_player_4", "boom_button_player_4", Color.yellow, _max_hp);
        _cameraShake = Camera.main.GetComponent<CameraShake>();
        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
        _position = new List<int>();
    }

    public void addPosition(Player player)
    {
        if (_player1 != null && player == _player1) _position.Add(1);
        if (_player2 != null && player == _player2) _position.Add(2);
        if (_player3 != null && player == _player3) _position.Add(3);
        if (_player4 != null && player == _player4) _position.Add(4);
    }

    public Dictionary<string, Dictionary<string, int>> getPlayerStats()
    {
        Dictionary<string, Dictionary<string, int>> result = new Dictionary<string, Dictionary<string, int>>();

        if(_player1 != null)
        {
            Dictionary<string, int> stats = new Dictionary<string, int>();
            stats.Add("Position", _position.IndexOf(1));
            stats.Add("Kills", _player1._score);
            result.Add("Red", stats);
        }
        if (_player2 != null)
        {
            Dictionary<string, int> stats = new Dictionary<string, int>();
            stats.Add("Position", _position.IndexOf(2));
            stats.Add("Kills", _player2._score);
            result.Add("Blue", stats);
        }
        if (_player3 != null)
        {
            Dictionary<string, int> stats = new Dictionary<string, int>();
            stats.Add("Position", _position.IndexOf(3));
            stats.Add("Kills", _player3._score);
            result.Add("Yellow", stats);
        }
        if (_player4 != null)
        {
            Dictionary<string, int> stats = new Dictionary<string, int>();
            stats.Add("Position", _position.IndexOf(4));
            stats.Add("Kills", _player4._score);
            result.Add("Green", stats);
        }

        return result;
    }

    public SatelliteManager getSatelliteManager()
    {
        return _satelliteManager;
    }

    public void update()
    {
        if (_player1 != null)
            _player1.update();
        if (_player2 != null)
            _player2.update();
        if (_player3 != null)
            _player3.update();
        if (_player4 != null)
            _player4.update();
    }
}
