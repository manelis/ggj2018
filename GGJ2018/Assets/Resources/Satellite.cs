﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Satellite : MonoBehaviour {

    public Player _occupyingPlayer;
    public GameObject _highLight;
    public GameObject _model;

    public bool _player1StartingPosition;
    public bool _player2StartingPosition;
    public bool _player3StartingPosition;
    public bool _player4StartingPosition;

    public Quaternion _startingRotation;

    public Bomb _bomb;

    void Start()
    {
        _startingRotation = Quaternion.identity;
        _occupyingPlayer = null;
        _bomb = null;

        GameplayManager.Instance.getSatelliteManager().AddSattelite(this);

        if (_player1StartingPosition)
        {
            GameplayManager.Instance.getSatelliteManager().movePlayerToSatelite(GameplayManager.Instance._player1, this);
            GameplayManager.Instance._player1._startingSatellite = this;
        }
        if(_player2StartingPosition)
        {
            GameplayManager.Instance.getSatelliteManager().movePlayerToSatelite(GameplayManager.Instance._player2, this);
            GameplayManager.Instance._player2._startingSatellite = this;
        }
        if (_player3StartingPosition)
        {
            GameplayManager.Instance.getSatelliteManager().movePlayerToSatelite(GameplayManager.Instance._player3, this);
            GameplayManager.Instance._player3._startingSatellite = this;
        }
        if (_player4StartingPosition)
        {
            GameplayManager.Instance.getSatelliteManager().movePlayerToSatelite(GameplayManager.Instance._player4, this);
            GameplayManager.Instance._player4._startingSatellite = this;
        }
    }

    void Update()
    {
        
    }

    public static float EaseInOutQuad(float start, float end, float value)
    {
        value /= .5f;
        end -= start;
        if (value < 1) return end * 0.5f * value * value + start;
        value--;
        return -end * 0.5f * (value * (value - 2) - 1) + start;
    }


    public void rotateTo(float xAxis, float yAxis)
    {
        if (xAxis == 0 && yAxis == 0)
            return;

        var forward = new Vector3(xAxis, yAxis, 0);
        forward.Normalize();

        float rot_y = Mathf.Atan2(-xAxis, yAxis) * Mathf.Rad2Deg;

        var rotationAngles = transform.rotation.eulerAngles;
        var targetRotation = Quaternion.Euler(0.0f, 0.0f, rot_y);
        var maxRotation = Mathf.Abs(Quaternion.Angle(_startingRotation, targetRotation));
        var currentRotation = maxRotation - Mathf.Abs(Quaternion.Angle(transform.rotation, targetRotation));

        var ease = EaseInOutQuad(0.3f, 1.0f, currentRotation / (maxRotation + float.Epsilon) * 0.5f);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, ease);

        if (currentRotation > maxRotation - 1f)
            _startingRotation = transform.rotation;

    }

    public Vector3 getDirection()
    {
        return transform.rotation * new Vector3(0.0f, 1.0f, 0.0f);
    }

    public void disableHighlight(){
        _highLight.SetActive(false);
    }

    public void highlight(Color highlightColor){
        _highLight.GetComponent<Renderer>().materials[2].color = highlightColor;
        _highLight.SetActive(true);
    }

    public void setColor(Color color)
    {
        _model.GetComponent<Renderer>().materials[1].color = color;
    }

    public void resetColor()
    {
        _model.GetComponent<Renderer>().materials[1].color = Color.grey;
    }

    public bool returnable()
    {
        return !_player1StartingPosition && !_player2StartingPosition && !_player3StartingPosition && !_player4StartingPosition;
    }
}
