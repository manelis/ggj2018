﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour {

    public Player _player;
    public Satellite _satellite;

    public float _lifeTime = 6.0f;


    private Quaternion _lastGeneratedRotation = Quaternion.identity;

	// Use this for initialization
	void Start ()
    {
    }
	
	// Update is called once per frame
	void Update ()
    {
        var angle = Quaternion.Angle(_lastGeneratedRotation, transform.rotation);
        if (angle <= 5f)
            generateRotation();

        transform.rotation = Quaternion.RotateTowards(transform.rotation, _lastGeneratedRotation, Time.deltaTime * 25f);

        float max = 1.2f;
        float min = 0.8f;
        float scale = ((Mathf.Cos(_lifeTime * 5) + 1.0f) / 2.0f * (max - min) + 1.0f - (max - min) * 0.5f) * 0.43f;
        transform.localScale = new Vector3(scale, scale, scale);

        _lifeTime -= Time.deltaTime;

        if (_lifeTime <= 0.0f)
        {
            if (_satellite._occupyingPlayer != null)
                _satellite._occupyingPlayer.die();
            Destroy(this.gameObject);
        }
    }

    private void generateRotation()
    {
        _lastGeneratedRotation = Random.rotation;
    }

    public void playerEntered()
    {
        if (_satellite._occupyingPlayer != null)
        {
            _satellite._occupyingPlayer.die();
            Destroy(this.gameObject);
        }
    }
}
