﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player {

    private string _horizontalAxis;
    private string _verticalAxis;
    private string _aButton;
    private string _bButton;

    private float _timeOnStartingSatellite;

    public Color _playerColor;

    public Satellite _currentSatellite;
    public Satellite _pointingSatellite;

    public Satellite _startingSatellite;

    public PlayerProjectile _currentProjectile;

    public Selector _selector;
    public LineRenderer _line;

    public int _current_hp;
    public int _score;
    public bool _dead;
    public float _currentDeadTime = 0;
    public float _coolDown = 0.0f;
    public float _coolDownMax = 3.0f;

    public Player(string horizontalAxis, string verticalAxis, string aButton, string bButton, Color color, int max_hp)
    {
        _horizontalAxis = horizontalAxis;
        _verticalAxis = verticalAxis;
        _aButton = aButton;
        _bButton = bButton;
        _playerColor = color;
        _current_hp = max_hp;

        _line = (GameObject.Instantiate(Resources.Load("LinePrefab")) as GameObject).GetComponent<LineRenderer>();
        _line.material.color = color;

        _selector = (GameObject.Instantiate(Resources.Load("SelectorPrefab")) as GameObject).GetComponent<Selector>();
        _selector.transform.gameObject.SetActive(false);

        init();
    }

    public void init()
    {
        _currentSatellite = null;
        _currentProjectile = null;

        _startingSatellite = null;
        _timeOnStartingSatellite = 0;

        _score = 0;
        _dead = false;
        _currentDeadTime = 0;
    }

    public Satellite getSatellite()
    {
        return _currentSatellite;
    }

    public bool isDead()
    {
        return _current_hp <= 0;
    }

    public void die()
    {
        if (_dead)
            return;

        GameObject destroyPrefab = Resources.Load("Destroy") as GameObject;
        GameObject destroyAnim = GameObject.Instantiate(destroyPrefab, _currentSatellite.transform.position, Quaternion.identity);

        _current_hp--;
        _currentSatellite._occupyingPlayer = null;
        _currentSatellite.resetColor();
        _currentSatellite = null;
        _selector.unSelect();
        _timeOnStartingSatellite = 0;
        if (_pointingSatellite != null)
        {
            _pointingSatellite.disableHighlight();
        }
        _dead = true;
        _line.gameObject.SetActive(false);
        GameplayManager.Instance._cameraShake.startShaking(0.025f);
        GameplayManager.Instance.addPosition(this);
        //GameplayManager.Instance._soundManager.playSound("Explosion");
    }

    public void addScore()
    {
        _score++;

    }

    public void respawn()
    {
        if (_current_hp <= 0)
            return;

        GameplayManager.Instance.getSatelliteManager().movePlayerToSatelite(this, _startingSatellite);
        _dead = false;
        _currentDeadTime = 0;
    }

    public void update()
    {
        if (_dead)
        {
            if (_currentDeadTime > 2)
                respawn();
            else
                _currentDeadTime += Time.deltaTime;
            return;
        }

        float horizontal = Input.GetAxis(_horizontalAxis);
        float vertical = Input.GetAxis(_verticalAxis);
        bool timeOnStartingSatelliteExpired = _timeOnStartingSatellite > 2.0f;
        bool fire = Input.GetButtonDown(_aButton) || timeOnStartingSatelliteExpired;
        bool fire2 = Input.GetButtonDown(_bButton);

        if (_currentSatellite != null)
        {
            _currentSatellite.rotateTo(horizontal, vertical);
            //var direction = new Vector3(horizontal, vertical, 0.0f);
            var direction = _currentSatellite.getDirection();
            Satellite newPointingSatellite;
            if (timeOnStartingSatelliteExpired)
            {
                newPointingSatellite = GameplayManager.Instance.getSatelliteManager().GetClosestSatellite(_currentSatellite);
            }
            else
            {
                newPointingSatellite = GameplayManager.Instance.getSatelliteManager().GetSatellite(direction, _currentSatellite);
            }

            _line.gameObject.SetActive(horizontal != 0 || vertical != 0 || newPointingSatellite != null);

            if(newPointingSatellite != _pointingSatellite && _pointingSatellite != null)
            {
                _pointingSatellite.disableHighlight();
            }

            if(newPointingSatellite != null)
            {
                newPointingSatellite.highlight(_playerColor);
            }

            _pointingSatellite = newPointingSatellite;

            if(!_currentSatellite.returnable())
            {
                _timeOnStartingSatellite += Time.deltaTime;
            }
            else
            {
                _timeOnStartingSatellite = 0;
            }

            _line.SetPosition(0, _currentSatellite.transform.position);

            if (newPointingSatellite != null)
            {

                _line.SetPosition(1, _currentSatellite.transform.position + direction * (newPointingSatellite.transform.position - _currentSatellite.transform.position).magnitude);
            }
            else
            {
                _line.SetPosition(1, _currentSatellite.transform.position + direction * 100f);
            }

            //BOOM
            if(fire2 && _coolDown <= 0.0f)
            {
                GameObject bomb = GameObject.Instantiate(Resources.Load("BombPrefab"), _currentSatellite.transform.position + new Vector3(0.0f,0.0f, -1.0f), Quaternion.identity) as GameObject;
                Bomb bombScript = bomb.GetComponent<Bomb>();
                bombScript._player = this;
                bombScript._satellite = _currentSatellite;
                _currentSatellite._bomb = bombScript;
                _coolDown = _coolDownMax;
            }

        }

        if ((fire || timeOnStartingSatelliteExpired) && _currentProjectile == null && _pointingSatellite != null)
        {
            commitPlayerToSatellite();
        }

        decrementBombCooldown();
    }

    private void decrementBombCooldown()
    {
        _coolDown -= Time.deltaTime;
        if (_coolDown < 0f)
        {
            _coolDown = 0f;
        }
    }

    public void commitPlayerToSatellite()
    {
        GameObject projectile = GameObject.Instantiate(Resources.Load("PlayerProjectilePrefab")) as GameObject;
        projectile.transform.position = _currentSatellite.transform.position;
        _currentProjectile = projectile.GetComponent<PlayerProjectile>();

        _currentProjectile._player = this;
        _currentProjectile.setTarget(_pointingSatellite);

        _currentSatellite.resetColor();
        _currentSatellite._occupyingPlayer = null;
        _currentSatellite = null;
        _line.gameObject.SetActive(false);
        _selector.unSelect();
    }
}
