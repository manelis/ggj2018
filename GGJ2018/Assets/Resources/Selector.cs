﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selector : MonoBehaviour {
    
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

    public void selectSatellite(Satellite satellite, Color color)
    {
        this.gameObject.SetActive(true);
        transform.position = satellite.transform.position + new Vector3(0.0f, 2 - 0f, 0.0f);
        transform.gameObject.GetComponent<Renderer>().materials[2].color = color;
    }

    public void unSelect()
    {
        this.gameObject.SetActive(false);
    }
}
