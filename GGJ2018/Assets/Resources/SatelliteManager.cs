﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SatelliteManager : MonoBehaviour {

    private List<Satellite> satellites;

    void Awake()
    {
        satellites = new List<Satellite>();
    }

    // Use this for initialization
    void Start () {
       
    }
	
	// Update is called once per frame
	void Update () {
        GameplayManager.Instance.update();
	}

    public void movePlayerToSatelite(Player player, Satellite newSatellite)
    {
        if (player._currentSatellite != null)
        {
            player._currentSatellite.resetColor();
            player._currentSatellite._occupyingPlayer = null;
            player._currentSatellite = null;
            player._selector.unSelect();
        }

        if(newSatellite._occupyingPlayer != null)
        {
            GameplayManager.Instance.destroyPlayer(player, newSatellite._occupyingPlayer);
        }

        player._currentSatellite = newSatellite;
        newSatellite.setColor(player._playerColor);
        newSatellite._occupyingPlayer = player;
        newSatellite.disableHighlight();

        player._selector.selectSatellite(newSatellite, player._playerColor);

        if(newSatellite._bomb != null)
            newSatellite._bomb.playerEntered();
    }

    public void AddSattelite(Satellite sat)
    {
        satellites.Add(sat);
    }


    public Satellite GetClosestSatellite(Satellite currentSat)
    {
        var smalestDistance = float.MaxValue;
        Satellite satelliteToReturn = null;
        foreach (Satellite s in satellites)
        {
            if (!s.returnable())
                continue;
            var distance = (s.transform.position - currentSat.transform.position).magnitude;
            if(distance < smalestDistance)
            {
                smalestDistance = distance;
                satelliteToReturn = s;
            }
        }
        return satelliteToReturn;
    }

    private Satellite raycast(Satellite currentSat, Vector3 direction)
    {
        RaycastHit hit;
        Debug.DrawRay(currentSat.transform.position, direction * 100f, Color.yellow);
        if (Physics.Raycast(currentSat.transform.position, direction, out hit, float.MaxValue, 1 << LayerMask.NameToLayer("Satellite")))
        {
            return hit.transform.GetComponent<Satellite>();
        }
        
        return null;
    }

    public Satellite GetSatellite(Vector3 direction, Satellite currentSat)
    {
        var smalestAngle = float.MaxValue;
        Satellite satelliteToReturn = null;
        foreach(Satellite s in satellites)
        {
            if (!s.returnable())
                continue;

            var angle = Mathf.Abs( Vector3.Angle(direction, s.transform.position - currentSat.transform.position));
            if(angle < smalestAngle)
            {
                smalestAngle = angle;
                satelliteToReturn = s;
            }
        }

        if (smalestAngle > 15.0f)
            return null;

        if(satelliteToReturn != null)
        {
            int variance = 7;

            List<Satellite> foundSatellites = new List<Satellite>();
            for(int i = -variance; i <= variance; i++)
            {
                Vector3 customDirection = Quaternion.Euler(0f, 0f, (float)i) * direction;
                Satellite sat = raycast(currentSat, customDirection);
                if(sat != null)
                {
                    foundSatellites.Add(sat);
                }
            }

            Satellite closestSatellite = null;
            if(foundSatellites.Count > 0)
            {
                foreach(Satellite s in foundSatellites)
                {
                    float distance = (s.transform.position - currentSat.transform.position).magnitude;
                    if(closestSatellite == null || distance < (closestSatellite.transform.position - currentSat.transform.position).magnitude)
                    {
                        closestSatellite = s;
                    }
                }

            }
            if (closestSatellite != null)
                satelliteToReturn = closestSatellite;
        }
        return satelliteToReturn;
    }
}
