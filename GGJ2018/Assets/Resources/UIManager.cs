﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public VictoryPopup _victoryPopup;
    public EndScreen _endScreen;

    private bool _ending = false;
    private float _victoryTime = 0;

    public void reset()
    {
        _ending = false;
        _victoryTime = 0;
        _victoryPopup.disable();
        _endScreen.gameObject.SetActive(false);
    }

    public void showVictoryScreen()
    {
        _ending = true;
        _victoryPopup.enable();
    }

    private void OnGUI()
    {
        if (!_ending)
        {
            if (GameplayManager.Instance.gameEnded())
            {
                showVictoryScreen();
            }
            else
            {
                return;
            }
        }

        _victoryTime += Time.deltaTime;
        if (_victoryTime > 4)
        {
            _victoryPopup.gameObject.SetActive(false);
            RestartGame();
        }
    }

    public void RestartGame()
    {
        StartCoroutine(LoadYourAsyncScene());
    }

    IEnumerator LoadYourAsyncScene()
    {
        // The Application loads the Scene in the background at the same time as the current Scene.
        //This is particularly good for creating loading screens. You could also load the Scene by build //number.
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Result");

        //Wait until the last operation fully loads to return anything
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}