﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadFromImage : MonoBehaviour {

	public Texture2D levelTexture;

	// Use this for initialization
	void Awake () {
		var numberOfStartingPlanets = 0;
		
		var map = GameObject.Find("Map");
		var curPosition = new Vector3(0f, 0f, 0f);
		for (int x = 0; x < levelTexture.width; x++)
		{
			for (int z = 0; z < levelTexture.height; z++)
			{
				curPosition = new Vector3(x + map.transform.position.x, z + map.transform.position.y, 0f);
				var pixel = levelTexture.GetPixel(x, z);


				if (pixel == Color.black)
				{
					Instantiate(Resources.Load("SatellitePrefab"), curPosition, Quaternion.identity, map.transform);
				}

				if (pixel == Color.red)
				{
					var gameObject = Instantiate(Resources.Load("PlanetPrefab"), curPosition, Quaternion.identity, map.transform) as GameObject;
					if (numberOfStartingPlanets == 0)
					{
						gameObject.GetComponent<Satellite>()._player1StartingPosition = true;
					}
					else if (numberOfStartingPlanets == 1)
					{
						gameObject.GetComponent<Satellite>()._player2StartingPosition = true;
					}
					else if (numberOfStartingPlanets == 2)
					{
						gameObject.GetComponent<Satellite>()._player3StartingPosition = true;
					}
					else if (numberOfStartingPlanets == 3)
					{
						gameObject.GetComponent<Satellite>()._player4StartingPosition = true;
					}

					numberOfStartingPlanets++;
				}
			}
		}

	}

	// Update is called once per frame
	void Update () {
		
	}
}
