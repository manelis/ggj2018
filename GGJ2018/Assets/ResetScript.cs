﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResetScript : MonoBehaviour {

    public bool is4players = false;

	// Use this for initialization
	void Awake ()
    {
        if(is4players)
            GameplayManager.Instance.restartGame4Players();
        else
            GameplayManager.Instance.restartGame();
    }
}
