﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BombCooldown : MonoBehaviour {

    public Image _bomb;
    public Image _bar;
    private Vector3 _barScale;
    public int _playerNum;

    private Player _player;
    
	void Start () {
        if (_playerNum == 1)
            _player = GameplayManager.Instance._player1;
        if (_playerNum == 2)
            _player = GameplayManager.Instance._player2;
        if (_playerNum == 3)
            _player = GameplayManager.Instance._player3;
        if (_playerNum == 4)
            _player = GameplayManager.Instance._player4;

        _barScale = _bar.transform.localScale;
    }
	
	void OnGUI () {
        if (_player._coolDown > 0)
        {
            _bomb.color = new Vector4(0.4f, 0.4f, 0.4f, 1.0f);
            _bar.transform.localScale = new Vector3(_player._coolDown / _player._coolDownMax *1.0f, _barScale.y, _barScale.z);
        }
        else
        {
            _bomb.color = Color.white;
            _bar.transform.localScale = new Vector3(0, _barScale.y, _barScale.z);
        }


    }
}
