﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndScreen : MonoBehaviour {

    private Dictionary<string, Dictionary<string, int>> _stats;
    public GameObject _statLine;

    private void Start()
    {
        _stats = GameplayManager.Instance.getPlayerStats();

        foreach (var item in _stats)
        {
            int position = item.Value["Position"];
            int kills = item.Value["Kills"];
            string player = item.Key;
            GameObject obj = Instantiate(_statLine, transform.position, Quaternion.identity, transform) as GameObject;
            obj.transform.position += new Vector3(0, 50 - position * 25, 0);
            obj.GetComponent<PlayerScoreLine>().update((position+2).ToString(), player, kills.ToString());
        }
    }

    private void OnGUI()
    {
        if (Input.GetButtonDown("Cancel"))
            RestartGame();
    }

    public void RestartGame()
    {
        StartCoroutine(LoadYourAsyncScene());
    }

    IEnumerator LoadYourAsyncScene()
    {
        // The Application loads the Scene in the background at the same time as the current Scene.
        //This is particularly good for creating loading screens. You could also load the Scene by build //number.
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Menu");

        //Wait until the last operation fully loads to return anything
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
