﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour {

    public float _xAngles;
    public float _yAngles;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(Vector3.up, _yAngles * Time.deltaTime);
        transform.Rotate(Vector3.right, _xAngles * Time.deltaTime);
    }
}
