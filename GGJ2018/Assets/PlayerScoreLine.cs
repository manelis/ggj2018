﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScoreLine : MonoBehaviour {

    public Text _position;
    public Text _player;
    public Text _kills;

    public void update(string position, string player, string kills)
    {
        _position.text = position;
        _player.text = player;
        _kills.text = kills;
    }
}
