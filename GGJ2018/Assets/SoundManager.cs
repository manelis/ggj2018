﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    public AudioClip _explosion;
    public AudioClip _movement;

    public void playSound(string sound)
    {
        if(sound == "Explosion")
        {
            GetComponent<AudioSource>().PlayOneShot(_explosion);
        }

        if (sound == "Move")
        {
            GetComponent<AudioSource>().PlayOneShot(_movement);
        }
    }
}
