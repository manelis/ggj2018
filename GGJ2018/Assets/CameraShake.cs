﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour {

    Vector3 originalCameraPosition;

    public Vector3 displacementVector;

    float shakeAmt = 0;

    public Camera mainCamera;

    public bool shaking;

    private void Start()
    {
        originalCameraPosition = mainCamera.transform.position;
        displacementVector = new Vector3(0.0f, 0.0f, 0.0f);
        shaking = false;

    }

    public void startShaking(float amount)
    {
        shaking = true;
        originalCameraPosition = mainCamera.transform.position;
        shakeAmt = amount;
        InvokeRepeating("ShakeCamera", 0, .01f);
        Invoke("StopShaking", 0.3f);
    }

    void ShakeCamera()
    {
        if (shakeAmt > 0)
        {
            float quakeAmt = Random.value * shakeAmt * 2 - shakeAmt;
            Vector3 pp = mainCamera.transform.position;
            pp.y += quakeAmt; // can also add to x and/or z
            pp.x += quakeAmt; // can also add to x and/or z
            mainCamera.transform.position = pp;
        }
    }

    void StopShaking()
    {
        shaking = false;
        CancelInvoke("ShakeCamera");
        mainCamera.transform.position = originalCameraPosition;
    }

    public void movementImpact(Vector3 direction){

        displacementVector += new Vector3(direction.x, direction.y, 0.0f).normalized * 0.4f;
    }

    private void Update()
    {
        if (shaking)
            return;

        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, new Vector3(displacementVector.x, displacementVector.y, originalCameraPosition.z), Time.deltaTime);
        displacementVector = Vector3.Lerp(displacementVector, new Vector3(0.0f, 0.0f, 0.0f), Time.deltaTime * 0.8f);
    }

}
